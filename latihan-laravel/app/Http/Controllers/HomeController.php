<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home()
    {
        return view('home');
    }
    public function masuk(Request $request)
    {
        $namaDepan = $request['depan'];
        $namaBelakang = $request['belakang'];

        return view('index',['namadepan' => $namaDepan, 'namabelakang' => $namaBelakang]);
    }
}
