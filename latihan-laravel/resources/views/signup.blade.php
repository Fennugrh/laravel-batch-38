<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Form Biodata</title>
</head>
<body>
<h2>Buat Account Baru!</h2>

<h3>Sign Up Form</h3>
<form action="/kirim" method="POST">
    @csrf
	<label>First Name :</label>
		<input type="text" name="depan"> <br><br>

	<label>Last Name :</label>
		<input type="text" name="belakang"> <br><br>

	<label>Gender :</label><br>
		<input type="radio" name="G">Male<br>
		<input type="radio" name="G">Female<br>
		<input type="radio" name="G">Other<br><br>

	<label>Nationality :</label>
		<select name="Nationality">
			<option value="Indonesian">Indonesian</option>
			<option value="Singapura">Singapura</option>
		</select><br><br>
	<label>Language Spoken :</label><br>
		<input type="checkbox" value="1" name="Language Spoken"> Bahasa Indonesia <br>
		<input type="checkbox" value="2" name="Language Spoken"> English <br>
		<input type="checkbox" value="3" name="Language Spoken"> Other <br><br>
	<label>Bio :</label><br>
		<textarea name="message" rows="10" cols="30"></textarea><br>
		<input type="submit" value="Sign Up">
</body>
</html>