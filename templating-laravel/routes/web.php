<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [IndexController::class, "utama"]);
Route::get('/table', [TableController::class, "table"]);
Route::get('/data-table', [DataTableController::class, "datatable"]);

// CRUD Cast

//Create Data
//Mengarah ke form tambah data
Route::get('/cast/create',[CastController::class, 'create']);
//Menyimpan data ke cast DB
Route::post('/cast', [CastController::class, 'store']);

//Read Data
//Mengambil semua data di database
Route::get('/cast', [CastController::class, 'index']);
//Detail cast ambil berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);

//Update Data
//Form edit data table Cast
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);
//Update data mengambil dari ID
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete Data
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);
