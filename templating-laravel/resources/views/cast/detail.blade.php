@extends('layouts.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('content')
<h1>{{$cast -> nama}}</h1>
<h5>{{$cast -> umur}} Tahun</h5>
<p>{{$cast -> bio}}</p>
@endsection