<?php
 	/**
 	 * 
 	 */
 		require_once('animal.php');
 		require_once('frog.php');
 		require_once('ape.php');

 		echo "Nama Hewan"."<br><br>";

 		$jenishewan = new Animal("Shaun");

 		echo "Name : " .$jenishewan->nama ."<br>";
 		echo "Legs : " .$jenishewan->legs ."<br>";
 		echo "Cold Blooded : " .$jenishewan->cold_blooded ."<br><br>";

 		$jenisfrog = new Frog("Buduk");

 		echo "Name : " .$jenisfrog->nama ."<br>";
 		echo "Legs : " .$jenisfrog->legs ."<br>";
 		echo "Cold Blooded : " .$jenisfrog->cold_blooded ."<br>";
 		echo "Jump : " .$jenisfrog->jump ."<br><br>";

 		$jenisape = new Ape("Kera Sakti");

 		echo "Name : " .$jenisape->nama ."<br>";
 		echo "Legs : " .$jenisape->leg ."<br>";
 		echo "Cold Blooded : " .$jenisape->cold_blooded ."<br>";
 		echo "Yell : " .$jenisape->yell ."<br><br>";
 ?>